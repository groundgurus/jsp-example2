<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello <%= request.getParameter("name") %>!</h1>
        <h1>Hello ${param.name}!</h1>
        <form method="POST" action="HelloServlet">
            Enter your name
            <input name="yourname"/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
